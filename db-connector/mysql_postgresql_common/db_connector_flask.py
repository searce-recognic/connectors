from flask import Flask,request,jsonify
from sqlalchemy import create_engine
from sqlalchemy import MetaData, Table, String, Column, Text, Integer
import time
import configparser


app = Flask(__name__)


@app.route('/api/v1/db/',methods = ['POST'])
def get_data():
    content_len = len(request.get_json())
    content = request.get_json()
    start_time = time.time()
    connect_db()
    end_time = time.time() - start_time

    # Response message
    return_response = {

        'Message':'Enter your custom message here',
        'Total data inserted': content_len,
        'Total Time': end_time
    }
    return jsonify(return_response)

def connect_db():
    """
        DB_TYPE LIST
        MYSQL       : mysql+mysqldb
        PostgreSQL  : postgresql+psycopg2
    """
    config = configparser.ConfigParser()
    config.read('dbConfig.cfg')

    DATABASE_TYPE     = config['databaseConfig']['db_type']                    # Database Type
    DATABASE_USERNAME = config['databaseConfig']['username']                   # Database username
    DATABASE_PASSWORD = config['databaseConfig']['password']                   # Database password
    HOST              = config['databaseConfig']['host']                       # Database IP address / URL
    PORT              = config['databaseConfig']['port']                       # Database port number
    DATABASE_NAME     = config['databaseConfig']['database_name']              # Database name
    TABLE_NAME        = config['databaseConfig']['table_name']                 # Database table name to be created.



    DATABASE_CONNECTION = f"{DATABASE_TYPE}://{DATABASE_USERNAME}:{DATABASE_PASSWORD}@{HOST}:{PORT}/{DATABASE_NAME}"
    # DATABASE_CONNECTION = ""
    print("Database Connection String ",DATABASE_CONNECTION)
    try:
        metadata = MetaData()
        # Table structure
        # Create table structure as per your requirement.
        # If any changes are made in table structure then make sure to change the 'insert_query' accordingly.
        table_schema = Table(
            TABLE_NAME, metadata,
                Column('id', Integer(), primary_key=True,autoincrement=True),
                Column('status',Text()),
                Column('system_status_code',Text()),
                Column('file_name',Text()),
                Column('result', Text(),nullable=False),
                Column('url',Text())
            )

        db_engine = create_engine(DATABASE_CONNECTION,echo=True)                # Create connection pool for database
        if not db_engine.dialect.has_table(db_engine, TABLE_NAME):    # Create table if not present in database
            metadata.create_all(db_engine)
            print("Tabel Created")

        # Insert Query as per table structure.
        insert_query = [
            {
                'status': str(x['response'][0]['status']),
                'system_status_code': str(x['response'][0]['system_status_code']),
                'file_name': str(x['response'][0]['file_name']),
                'result': str(x['response'][0]['result']),
                'url': str(x['response'][0]['url'])
            }
            for x in request.get_json()
        ]

        db_connection = db_engine.connect()
        db_transaction = db_connection.begin()
        # Insert data into database.
        try:
            db_connection.execute(table_schema.insert(),insert_query)
            db_transaction.commit()
        except:
            db_transaction.rollback()
            raise
    except Exception as e:
        raise


if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
